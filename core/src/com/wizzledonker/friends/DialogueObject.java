package com.wizzledonker.friends;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Winfried on 15/06/2016.
 */
public class DialogueObject {
    GameObject speechParent;

    boolean startedPlaying = false;

    float timeElapsed = 0;
    float lineTimer = 0;

    float activationRange = 100f;
    int lastLine;

    int lineDuration = 2;

    Map<Integer, String> chat = new HashMap<Integer, String>();
    Map<Integer, String> playerChat = new HashMap<Integer, String>();
    public DialogueObject(Map<Integer, String> chat) {
        super();
        this.chat = chat;
    }

    public void setPlayerChat(Map<Integer, String> chat) {
        this.playerChat = chat;
    }

    public void addPlayerChatLine(int i, String s) {
        this.playerChat.put(i, s);
    }

    public void setSpeechParent(GameObject g) {
        this.speechParent = g;
    }

    public void addDialogueLine(int seconds, String line) {
        chat.put(seconds, line);
    }

    public void setDialogue(Map<Integer, String> chat) {
        this.chat = chat;
    }

    public void setActivationRange(float dist) {
        activationRange = dist;
    }

    public void setLineDuration(int time) {
        lineDuration = time;
    }

    public int getDuration() {
        int result = 0;
        for (int time : chat.keySet()) {
            if (time > result) result = time;
        }
        for (int time : playerChat.keySet()) {
            if (time > result) result = time;
        }
        return result + this.lineDuration;
    }

    public String getCurrentText(float delta) {
        if (Friends.player.getWorldPosition().dst(speechParent.getWorldPosition()) < activationRange) {
            timeElapsed += delta;

            //Current stage in the animation
            float timeCheck = timeElapsed % getDuration();

            if (timeElapsed/getDuration() < 1) {
                int key = -1;
                for (int time : chat.keySet()) {
                    if (timeCheck > time && timeCheck < time + lineDuration) {
                        key = time;
                    }
                }

                int playKey = -1;
                for (int time : playerChat.keySet()) {
                    if (timeCheck > time && timeCheck < time + lineDuration) {
                        playKey = time;
                    }
                }

                if (key >= 0 || playKey >= 0) {
                    if (!playerChat.isEmpty()) {
                        Friends.player.setChatText(playerChat.get(playKey));
                    }
                    return chat.get(key);
                } else {
                    Friends.player.setChatText(null);
                }
            }
        } else {
            if (timeElapsed/getDuration() > 1) {
                timeElapsed = 0;
            }
        }
        Friends.player.setChatText(null);
        return null;
    }
}
