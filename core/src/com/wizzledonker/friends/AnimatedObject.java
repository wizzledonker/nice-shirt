package com.wizzledonker.friends;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

/**
 * Created by Winfried on 31/05/2016.
 */
public class AnimatedObject extends GameObject {
    TextureAtlas textureAtlas;
    Animation animation;

    int numFrames;
    boolean looping;
    float lifeTimer;

    public AnimatedObject(float x, float y, int endFrame, int startFrame, TextureAtlas object, boolean loop, float fps) {
        //Behave the superseding class
        super(x, y, object.findRegion("0001"));

        this.numFrames = endFrame;
        textureAtlas = object;

        TextureRegion[] text = new TextureRegion[numFrames-startFrame+1];
        for (int i = 0; i < text.length; ++i) {
            text[i] = textureAtlas.findRegion("000"+(i+startFrame));
        }

        animation = new Animation(fps, text);
        looping = loop;
    }

    @Override
    public void draw(Batch batch, float alpha) {
        if (layer == 0) {
            lifeTimer += Gdx.graphics.getDeltaTime();
            batch.draw(animation.getKeyFrame(lifeTimer, looping), getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), this.getScaleX(), this.getScaleY(), getRotation());

            this.drawFont(batch);
        }
    }

    @Override
    public void drawLayer(Batch batch, float a) {
        //This is used by the terrain if the layer of the object is higher than zero
        lifeTimer += Gdx.graphics.getDeltaTime();
        batch.draw(animation.getKeyFrame(lifeTimer, looping), getX() + (layer == 0 ? 0 : ((Friends.stage.getCamera().position.x / (Friends.depth * layer) - 100f * Friends.scaleFactor))), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), this.getScaleX(), this.getScaleY(), getRotation());
    }
}
