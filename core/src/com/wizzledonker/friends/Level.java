package com.wizzledonker.friends;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.wizzledonker.friends.util.Line2D;
import com.badlogic.gdx.math.Intersector;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by Winfried on 30/05/2016.
 */
public class Level extends Actor {
    //The game level class, that can implement as many layers as needed

    //Level variables, layers is a sorted list going from highest to lowest
    public List<TextureRegion> layers = new ArrayList<TextureRegion>();

    //Barriers
    public Line2D[] platforms;
    public Line2D[] barriers;
    public Line2D[] grabbables;

    public Level(List<TextureRegion> tLayers, Line2D[] tPlatforms, Line2D[] barriers, Line2D[] grabbables) {
        this.grabbables = grabbables;
        this.platforms = tPlatforms;
        this.barriers = barriers;
        this.layers = tLayers;
    }

    public void draw(Batch batch, float alpha) {
        //Draw all the layers
        for (int i = layers.size()-1; i > -1; --i) {
            float leftLocation = 1.0f;

            if (i > 0) {
                int layWidth = layers.get(i).getRegionWidth();
                int levWidth = layers.get(0).getRegionWidth();
                leftLocation = (float) (layWidth*Math.pow(layWidth/levWidth, i))/levWidth;
                System.out.println("Lay: " + layWidth + ", Lev: " + levWidth + ", Loc: " + leftLocation);
            }

            batch.draw(layers.get(i), (0-Friends.stage.getCamera().position.x)+Friends.stage.getCamera().position.x*leftLocation, getY(), getOriginX(), getOriginY(), layers.get(i).getRegionWidth(), layers.get(i).getRegionHeight(), getScaleX(), getScaleY(), getRotation());

            //Draw the uninitiated
            for (Actor child : this.getParent().getChildren()) {
                if (child instanceof GameObject) {
                    GameObject childG = (GameObject) child;
                    if (childG.layer > 0 && (childG.layer == layers.size()-i)) {
                        childG.drawLayer(batch, alpha);
                    }
                }
            }
        }
    }

    public Set<Float> getPlatformsAtWorldLocation(float x, float y, float width, float search) {
        //This function will return where the ground is for a given location

        Set<Float> locs = new HashSet<Float>();

        Polygon playerPoly = new Polygon( new float[] {x, y, x+width, y, x+width, y+search, x, y+search});
        //playerPoly.setOrigin(x, 0);

        for (Line2D platform : platforms) {
            if (Intersector.intersectSegmentPolygon(platform.start, platform.end, playerPoly)) {
                locs.add((float) platform.getY1());
            }
        }

        return locs;
    }

    public Set<Float> getGrabbablesAtWorldLocation(float x, float y, float width, float search) {
        //This function will return where the ground is for a given location

        Set<Float> locs = new HashSet<Float>();

        Polygon playerPoly = new Polygon( new float[] {x, y+64-search, x+width, y+64-search, x+width, y+64, x, y+64});
        //playerPoly.setOrigin(x, 0);

        for (Line2D grabbable : grabbables) {
            if (Intersector.intersectSegmentPolygon(grabbable.start, grabbable.end, playerPoly)) {
                locs.add((float) grabbable.getIntersectionY(x));
            }
        }

        return locs;
    }

    public Set<Float> getBarriersAtWorldLocation(float x, float y, float width, float search) {
        //This function will return where the ground is for a given location
        Set<Float> locs = new HashSet<Float>();

        for (Line2D barrier : barriers) {
            if (barrier.intersectsWith(x-2, y, x+width, y)) {
                locs.add((float) barrier.getX1());
            }
        }

        return locs;
    }

    @Override
    public void act(float deltaTime) {

    }
}
