package com.wizzledonker.friends;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

/**
 * Created by Winfried on 19/06/2016.
 */
public class TiledGameObject extends GameObject{

    private int offset = 0;

    private float movementSpeed = 0;

    //The TiledGameObject class defines a gameObject that only has a y component, as it moves according to a repetitive movement
    public TiledGameObject(float worldx, float worldy, TextureRegion texture) {
        super(worldx, worldy, texture);

        //Set the texture to be repeating
        texture.getTexture().setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
        texture.setRegionWidth((int) Friends.stage.getViewport().getScreenWidth());

        this.setScaleX(4f);

        this.setBounds(0, 0, Friends.stage.getViewport().getScreenWidth(), this.getHeight());
    }

    @Override
    public void act(float delta) {
        this.moveAlong(movementSpeed*delta);
    }

    public void setMovementSpeed(float speed) {
        this.movementSpeed = speed;
    }

    public void moveAlong(float amount) {
        offset += amount;
        texture.scroll(amount, 0);
    }
}
