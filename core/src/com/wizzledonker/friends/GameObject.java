package com.wizzledonker.friends;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;

/**
 * Created by Winfried on 02/06/2016.
 */
//The GameObject class is only responsible for the base mechanics
public class GameObject extends Actor {
    TextureRegion texture;

    int layer = 0;

    float worldX = 0;
    float worldY = 0;

    public DialogueObject dialogue = null;
    BitmapFont dialogueFont;

    public GameObject(float worldx, float worldy, TextureRegion texture) {
        this.setWorldX(worldx);
        this.setWorldY(worldy);

        this.texture = texture;

        this.dialogueFont = new BitmapFont(Gdx.files.internal(Friends.resLoc + "font.fnt"));
        this.dialogueFont.getData().setScale(Friends.scaleFactor / 2);
        this.setBounds(getX(), getY(), texture.getRegionWidth()*Friends.scaleFactor, texture.getRegionHeight()*Friends.scaleFactor);
    }

    public void updateBounds() {
        this.setBounds(getX(), getY(), texture.getRegionWidth()*Friends.scaleFactor, texture.getRegionHeight()*Friends.scaleFactor);
        this.setX(worldX*Friends.scaleFactor);
        this.setY(worldY*Friends.scaleFactor);
        this.dialogueFont.getData().setScale(Friends.scaleFactor/2);
    }

    public DialogueObject getDialogue() {
        return dialogue;
    }

    public void addDialogue(DialogueObject di) {
        di.setSpeechParent(this);
        this.dialogue = di;
    }

    public void setLayer(int layer) {
        this.layer = layer;
    }

    @Override
    public void draw(Batch batch, float a) {
        if (layer == 0) {
            batch.draw(texture, getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), this.getScaleX(), this.getScaleY(), getRotation());

            drawFont(batch);
        }
    }

    protected void drawFont(Batch batch) {
        if (this.dialogue != null) {
            String text = dialogue.getCurrentText(Gdx.graphics.getDeltaTime());
            if (text != null) {
                this.dialogueFont.draw(batch, text, getX()+this.getWidth()/2 - (this.dialogueFont.getXHeight() * text.length() / 2), getY() + this.getHeight());
            }
        }
    }

    public void drawLayer(Batch batch, float a) {
        //This is used by the terrain if the layer of the object is higher than zero
        batch.draw(texture, getX()+(Friends.stage.getCamera().position.x/(Friends.depth*layer))-100f*Friends.scaleFactor, getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), this.getScaleX(), this.getScaleY(), getRotation());
    }

    public void setWorldX(float x) {
        worldX = x;
        this.setX(x*Friends.scaleFactor);
    }

    public void setWorldY(float y) {
        worldY = y;
        this.setY(y*Friends.scaleFactor);
    }

    public void translateWorld(float x, float y) {
        this.setWorldX(this.worldX + x);
        this.setWorldY(this.worldY + y);
    }

    public Vector2 getWorldPosition() {
        return new Vector2(this.getWorldX(), this.getWorldY());
    }

    public float getWorldX() {
        return worldX;
    }

    public float getWorldY() {
        return worldY;
    }

    public float getWorldWidth() {
        return this.getWidth()/Friends.scaleFactor;
    }

    public float getWorldHeight() {
        return this.getHeight()/Friends.scaleFactor;
    }

    public void setWorldPosition(float x, float y) {
        this.setWorldX(x);
        this.setWorldY(y);
    }
}
