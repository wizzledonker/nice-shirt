package com.wizzledonker.friends;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;

import java.util.Set;

/**
 * Created by Winfried on 30/05/2016.
 */

//Implement the Actor class to make use of sprite features
public class Player extends GameObject {
    TextureAtlas textureAtlas;
    private String headText;

    //Animations
    float lifeTimeSeconds = 0;
    float grabTimeSeconds = 0;
    float climbTimeSeconds = 0;

    boolean scalingStarted = false;
    boolean climbingStarted = false;

    float climbingDestination = 0;

    Animation walkAnimation;
    Animation jumpAnimation;
    Animation jumpWalkAnimation;
    Animation grabAnimation;
    Animation climbAnimation;

    //Player movement properties
    boolean walkLeft = false;
    boolean walking = false;
    boolean isJumping = false;
    boolean scaling = false;
    boolean climbing = false;
    boolean ableToClimb = false;
    boolean drop = false;

    float lastGround = 0.0f;
    float moveSpeed = 70.0f;
    float scalingRange = 16f;
    float climbRange = 20f;

    float otherg = 0;

    //Physics settings
    public float jumpVelocity = 170; //Jump velocity in metres per second
    public float currentJumpVelocity = 0;

    public float playerHeight = 1.66f; //Height of the player in metres. Player is 5 foot 6 tall (same as me IRL).

    private double timeFalling = 0;
    private float timeJumping = 0;

    private float jumpWarmup = 0;

    public Player(float posx, float posy, TextureAtlas texture) {
        super(posx, posy, texture.findRegion("0001"));
        this.registerTouchListeners();

        textureAtlas = texture;

        //Init single press events
        //this.registerKeyPress();

        //Initialise the player animation system (always 8 frames per walk cycle)
        TextureRegion[] walkFrames = new TextureRegion[8];
        for (int i = 2; i <= 9; ++i) {
            walkFrames[i - 2] = (textureAtlas.findRegion("000" + i));
        }
        walkAnimation = new Animation(1 / 12f, walkFrames);

        //Jump frames
        TextureRegion[] jumpFrames = new TextureRegion[2];
        jumpFrames[0] = (textureAtlas.findRegion("0010"));
        jumpFrames[1] = (textureAtlas.findRegion("0011"));

        jumpAnimation = new Animation(1 / 2f, jumpFrames);

        //Jump walk frames
        TextureRegion[] jumpWalkFrames = new TextureRegion[2];
        jumpWalkFrames[0] = (textureAtlas.findRegion("0013"));
        jumpWalkFrames[1] = (textureAtlas.findRegion("0014"));

        jumpWalkAnimation = new Animation(1 / 2f, jumpWalkFrames);

        //Grab frames
        TextureRegion[] grabFrames = new TextureRegion[3];
        grabFrames[0] = (textureAtlas.findRegion("0016"));
        grabFrames[1] = (textureAtlas.findRegion("0017"));
        grabFrames[2] = (textureAtlas.findRegion("0018"));

        grabAnimation = new Animation(1 / 16f, grabFrames);

        //Climb frames
        TextureRegion[] climbFrames = new TextureRegion[3];
        climbFrames[0] = (textureAtlas.findRegion("0019"));
        climbFrames[1] = (textureAtlas.findRegion("0020"));
        climbFrames[2] = (textureAtlas.findRegion("0021"));

        climbAnimation = new Animation(1/12f, climbFrames);

    }

    @Override
    public void draw(Batch batch, float alpha) {
        //Add to the life time
        lifeTimeSeconds += Gdx.graphics.getDeltaTime();

        //Get the frame to draw
        TextureRegion tex;
        tex = textureAtlas.findRegion("0001");

        if (walking) {
            tex = walkAnimation.getKeyFrame(lifeTimeSeconds, true);
        }

        if (isJumping) {
            float jumpTime = 0;
            if (this.jumpWarmup > 0.1) {
                jumpTime = 0.6f;
            }
            if (walking) {
                tex = jumpWalkAnimation.getKeyFrame(jumpTime, false);
            } else {
                tex = jumpAnimation.getKeyFrame(jumpTime, false);
            }
        }

        if (this.getVelocityY() > 0) {
            //Set the image for falling (whether stationary or walking)
            if (walking) {
                tex = textureAtlas.findRegion("0015");
            } else {
                tex = textureAtlas.findRegion("0012");
            }
        }

        //Whether the player is scaling the wall or not
        if (this.scaling) {
            if (!scalingStarted) {
                //If the player has just changed their y platform, set the animation to restart
                grabTimeSeconds = 0;
                scalingStarted = true;
            }
            grabTimeSeconds += Gdx.graphics.getDeltaTime();
            tex = grabAnimation.getKeyFrame(grabTimeSeconds, false);
        }

        //Climbing animation takes precedence over all (it is the only character-stopping animation
        if (this.climbing) {
            if (!climbingStarted) {
                climbTimeSeconds = 0;
                climbingStarted = true;
            }
            if (!climbAnimation.isAnimationFinished(climbTimeSeconds)) {
                climbTimeSeconds += Gdx.graphics.getDeltaTime();
                tex = climbAnimation.getKeyFrame(climbTimeSeconds, false);
            } else {
                climbing = false;
                this.setWorldY(this.climbingDestination+1);
            }
        }

        batch.draw(tex.getTexture(), getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), this.getScaleX(), this.getScaleY(), getRotation(), tex.getRegionX(), tex.getRegionY(),
                tex.getRegionWidth(), tex.getRegionHeight(), this.walkLeft, false);

        if (headText != null) {
            this.dialogueFont.draw(batch, headText, getX()+this.getWidth()/2 - (this.dialogueFont.getXHeight() * headText.length() / 2), getY() + this.getHeight());
        }
    }

    public void setChatText(String text) {
        this.headText = text;
    }

    @Override
    public void act(float modifier) {
        walking = false;

        float playerSize = this.getWorldHeight();

        if(Gdx.input.isKeyPressed(Input.Keys.R)){
            this.respawn();
            this.climbing = false;
            this.timeFalling = 0;
        }

        if (!climbing) {
            //Jump button
            if (Gdx.input.isKeyPressed(Input.Keys.UP)) {
                this.jump(jumpVelocity);
            }

            //First, the system for jumping
            if (isJumping) {
                this.jumpWarmup += modifier;
                if (jumpWarmup > 0.1) {
                    timeJumping += modifier;
                    float deltaY = (float) (currentJumpVelocity * modifier - timeJumping * Friends.gravity);
                    if (deltaY < 0) {
                        //At the top of jumping arc
                        isJumping = false;
                        timeJumping = 0;
                        this.jumpWarmup = 0;
                    } else {
                        this.translateWorld(0, deltaY);
                    }
                }
            }

            //Control system


            //Make sure there are no barriers in range
            Set<Float> barriers = Friends.level.getBarriersAtWorldLocation(this.getWorldX(), this.getWorldY(), this.getWorldWidth(), this.getWorldHeight());

            boolean noLeft = false;
            boolean noRight = false;
            for (float barrier : barriers) {
                if (barrier < this.getWorldX()) {
                    noLeft = true;
                } else {
                    noRight = true;
                }
            }

            if (!this.scaling) {
                if (Gdx.input.isKeyPressed(Input.Keys.RIGHT) || Gdx.input.isTouched()) {
                    if ((Gdx.input.isTouched() && Gdx.input.getX() > (Friends.stage.getViewport().getScreenWidth() / 2 + Friends.camThreshold*Friends.scaleFactor)) || Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
                        if (!noRight) {
                            this.translateWorld(moveSpeed * modifier, 0);
                            walking = true;
                            walkLeft = false;
                        }
                    }
                }

                if (Gdx.input.isKeyPressed(Input.Keys.LEFT) || Gdx.input.isTouched()) {
                    if ((Gdx.input.isTouched() && Gdx.input.getX() < (Friends.stage.getViewport().getScreenWidth() / 2 - Friends.camThreshold*Friends.scaleFactor)) || Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
                        if (!noLeft) {
                            this.translateWorld(-moveSpeed * modifier, 0);
                            walking = true;
                            walkLeft = true;
                        }
                    }
                }
            }

            Set<Float> levels = Friends.level.getPlatformsAtWorldLocation(this.getWorldX(), this.getWorldY(), this.getWorldWidth(), this.getWorldHeight());

            //Resetting the ability to do as such.
            scaling = false;
            ableToClimb = false;

            float frameRange = this.getVelocityY()*Gdx.graphics.getDeltaTime()*100+5;

            float groundLevel = -64;
            otherg = 0;
            for (float level : levels) {
                //Offset the level by the range achieved in a single frame and perform a test
                if ((level - frameRange) < this.getWorldY()) {
                    groundLevel = level;
                } else {
                    otherg = level;
                }
            }

            float grabLocation = this.getWorldHeight()/6;

            Set<Float> grabbables = Friends.level.getGrabbablesAtWorldLocation(this.getWorldX(), this.getWorldY(), this.getWorldWidth(), grabLocation);

            if (!this.drop) {
                if (grabbables.size() > 0) {
                    for (float grab : grabbables) {
                        groundLevel = grab - 58;
                        scaling = true;
                        if (grab + climbRange > otherg && otherg+this.getWorldHeight() > grab) {
                            ableToClimb = true;
                        }
                        if (!scalingStarted) {
                            this.grabTimeSeconds = 0;
                            scalingStarted = true;
                        }
                    }
                }
            }

            if (Math.abs(lastGround-groundLevel) > 0) {
                this.scalingStarted = false;
            }

            lastGround = groundLevel;

            //apply gravity to the player
            if (!isJumping) {
                if (getWorldY() > groundLevel) {
                    timeFalling += modifier;
                    this.translateWorld(0, (float) -((playerSize / playerHeight) * this.getVelocityY() * modifier));
                } else {
                    if (timeFalling != 0) {
                        timeFalling = 0;
                    }
                    this.setWorldY(groundLevel);
                }

                if (this.getWorldY() < -65) {
                    respawn();
                }
            }

            if (Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
                climb();
            }
        }
    }

    public void jump(float velocity) {
        if (!isJumping && (timeFalling == 0)) {
            this.currentJumpVelocity = velocity;
            isJumping = true;
        }
    }

    public void climb() {
        if (this.scaling) {
            if (ableToClimb) {
                this.climbing = true;
                this.climbingStarted = false;
                this.climbingDestination = otherg;
            }
        }
    }

    public float getVelocityY() {
        return Friends.gravity*((float) this.timeFalling);
    }

    public void respawn() {
        this.setWorldY(200);
    }

    private void registerTouchListeners() {
        this.addListener(new ActorGestureListener() {
            public void fling(InputEvent event, float vX, float vY, int button) {
                //Jump implementation
                if (vY > vX) {
                    drop = false;
                    climb();
                    if (!climbing) {
                        jump(jumpVelocity);
                    }
                } else {
                    drop = true;
                }
            }
        });
    }
}
