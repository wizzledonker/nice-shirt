package com.wizzledonker.friends.util;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * Created by Winfried on 03/06/2016.
 */
public class Line2D {

    public Vector2 start;
    public Vector2 end;

    public Line2D(float x1, float y1, float x2, float y2) {
        this.start = new Vector2(x1, y1);
        this.end = new Vector2(x2, y2);
    }

    public float getX1() {
        return this.start.x;
    }

    public float getY1() {
        return this.start.y;
    }

    public boolean intersectsWith(float x3, float y3, float x4, float y4) {
        return ((relativeCCW(start.x, start.y, end.x, end.y, x3, y3) *
                relativeCCW(start.x, start.y, end.x, end.y, x4, y4) <= 0)
                && (relativeCCW(x3, y3, x4, y4, start.x, start.y) *
                relativeCCW(x3, y3, x4, y4, end.x, end.y) <= 0));
    }

    public float getIntersectionY(float x) {
        float grad = (end.y-start.y)/(end.x-start.x);
        return (x-start.x)*grad+start.y;
    }

    public int relativeCCW(double x1, double y1,
                                  double x2, double y2,
                                  double px, double py) {
        x2 -= x1;
        y2 -= y1;
        px -= x1;
        py -= y1;
        double ccw = px * y2 - py * x2;
        if (ccw == 0.0) {

            ccw = px * x2 + py * y2;
            if (ccw > 0.0) {

                px -= x2;
                py -= y2;
                ccw = px * x2 + py * y2;
                if (ccw < 0.0) {
                    ccw = 0.0;
                }
            }
        }
        return (ccw < 0.0) ? -1 : ((ccw > 0.0) ? 1 : 0);
    }
}
