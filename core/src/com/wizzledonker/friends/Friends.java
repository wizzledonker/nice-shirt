package com.wizzledonker.friends;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.wizzledonker.friends.util.Line2D;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.delay;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.fadeOut;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.moveBy;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.repeat;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.run;
import static com.badlogic.gdx.scenes.scene2d.actions.Actions.sequence;

public class Friends extends ApplicationAdapter {
    public State state = State.PAUSE;
    public static Stage stage;
    public static Stage uiStage;
    public AssetManager asm = new AssetManager();

    private TextureRegion menuBackground;
    private Music mainMenu;
    public TextureAtlas charTex;
    private Group additions;

    public static Table uiBody;

    Label fpsLabel;

    public static final String version = "1.6 Alpha";

    //This will control how much of the horizontal map will be visible, based on the aspect ratio. Eventually fog will even the playing field.
    public static float scaleFactor;

    public static Level level;

    //Physics
    public static float gravity = 9.8f; //Given in ms^-2
    public static float depth = 4f;

    //Camera Settings
    public static float camThreshold = 50f;
    public float camSnappiness = 1/1.2f;
    public float worldMin = 0;
    public float worldMax = 1000;

    //Debug settings overlay
    boolean debug = false;

    boolean init = false;

    public static Player player;

    public static  String resLoc = "assets/";

    private Label loadProg;

    public void loadAssets() {
        //Level loads
        asm.load(resLoc + "forest1.gif", Texture.class);
        asm.load(resLoc + "forest2.gif", Texture.class);
        asm.load(resLoc + "forest0.gif", Texture.class);

        asm.load(resLoc + "sunset.png", Texture.class);
        asm.load(resLoc + "callum.atlas", TextureAtlas.class);
        asm.load(resLoc + "win.atlas", TextureAtlas.class);
        asm.load(resLoc + "waterfall.atlas", TextureAtlas.class);
        asm.load(resLoc + "butterfly.atlas", TextureAtlas.class);

        //User interface
        asm.load(resLoc + "play_0.png", Texture.class);
        asm.load(resLoc + "play_1.png", Texture.class);
        asm.load(resLoc + "title.png", Texture.class);
        asm.load(resLoc + "options_0.png", Texture.class);
        asm.load(resLoc + "options_1.png", Texture.class);
        asm.load(resLoc + "credit.png", Texture.class);
        asm.load(resLoc + "tileLand.png", Texture.class);
        asm.load(resLoc + "uiskin.json", Skin.class);

        //Music
        asm.load(resLoc + "audio/menu.ogg", Music.class);

    }

    @Override
    public void create() {
        loadAssets();
        uiStage = new Stage(new ScreenViewport());
        stage = new Stage(new ScreenViewport());

        loadProg = new Label("", new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        //loadProg.setPosition(uiStage.getWidth()/2-loadProg.getPrefWidth(), uiStage.getHeight()/2);
        loadProg.setAlignment(Align.center);
        loadProg.setFillParent(true);
        uiStage.addActor(loadProg);
    }

    public void init () {
        loadProg.remove();

        TextureRegion levelTexture = new TextureRegion(asm.get(resLoc + "forest0.gif", Texture.class));
        TextureRegion middleTexture = new TextureRegion(asm.get(resLoc + "forest1.gif", Texture.class));
        TextureRegion bgTexture = new TextureRegion(asm.get(resLoc + "forest2.gif", Texture.class));

        menuBackground = new TextureRegion(asm.get(resLoc + "sunset.png", Texture.class));

        TextureAtlas callumtex = asm.get(resLoc + "callum.atlas", TextureAtlas.class);

        //Create the stage to set our saga on
        Gdx.input.setInputProcessor(stage);

        //Layers to the level (this will be done dynamically
        List<TextureRegion> layers = new ArrayList<TextureRegion>();
        layers.add(0, levelTexture);
        layers.add(1, middleTexture);
        layers.add(2, bgTexture);

        Line2D[] plats = new Line2D[5];
        plats[0] = new Line2D(0, 70, 835, 70);
        plats[1] = new Line2D(0, 80, 83, 80);
        plats[2] = new Line2D(820, 16, 945, 16);
        plats[3] = new Line2D(432, 126, 650, 126);
        plats[4] = new Line2D(570, 180, 600, 180);

        Line2D[] barriers = new Line2D[2];
        barriers[0] = new Line2D(825, 0, 825, 69);
        barriers[1] = new Line2D(945, 0, 945, 100);

        Line2D[] grabbables = new Line2D[3];
        grabbables[0] = new Line2D(432, 146, 650, 146);
        grabbables[1] = new Line2D(830, 94, 945, 125);
        grabbables[2] = new Line2D(570, 204, 600, 204);

        //Add the level
        level = new Level(layers, plats, barriers, grabbables);
        //Json json = new Json();
        //System.out.println(json.prettyPrint(level));
        level.setBounds(level.getX(), level.getY(), levelTexture.getRegionWidth(), levelTexture.getRegionHeight());
        scaleFactor = (stage.getViewport().getScreenHeight()/level.getHeight());
        level.setScale(scaleFactor);

        GameObject callum = new GameObject(855, 20, callumtex.findRegion("0010"));

        Map<Integer, String> callumLines = new HashMap<Integer, String>();
        callumLines.put(0, "Hey...");
        callumLines.put(3, "I left my towel up on the hill...");
        callumLines.put(9, "Can you climb to it?");

        //GameObject nick = new GameObject(940, 95, new TextureRegion(new Texture(asm.get(resLoc + "dickolas.png"))));
        //callum.scaleBy(0.5f);

        //Add items to the scene
        AnimatedObject waterfall = new AnimatedObject(860, 55, 3, 1, asm.get(resLoc + "waterfall.atlas", TextureAtlas.class), true, 1/6f);
        AnimatedObject butterfly = new AnimatedObject(740, 76, 6, 1, asm.get(resLoc + "butterfly.atlas", TextureAtlas.class), true, 1/5f);
        //AnimatedObject lucy = new AnimatedObject(530, 127, 4, 1, new TextureAtlas(asm.get(resLoc + "pumpkin.atlas")), true, 1/6f);

        DialogueObject diag = new DialogueObject(callumLines);

        diag.addPlayerChatLine(6, "... why?");
        diag.addPlayerChatLine(12, "Sure thing.");

        callum.addDialogue(diag);

        waterfall.setLayer(2);

        //group items together
        Group land = new Group();
        land.addActor(level);
        land.addActor(waterfall);
        //land.addActor(lucy);
        land.addActor(butterfly);
        land.addActor(callum);
        //land.addActor(nick);

        charTex = asm.get(resLoc + "win.atlas", TextureAtlas.class);

        player = new Player(205, 120, charTex);

        stage.addActor(land);
        stage.addActor(player);

        stage.getViewport().getCamera().translate(500f*this.scaleFactor, 0, 0);

        this.setupUserInterface();

        //Add a listener for debug settings
        stage.addListener(new InputListener() {
            @Override
            public boolean keyDown (InputEvent event, int keyCode){
                if (keyCode == Input.Keys.D) {
                    debug = !debug;
                    fpsLabel.setVisible(debug);
                    return false;
                }
                return true;
            }
        });

    }

    private void setupUserInterface() {
        //Done with the Level, now sort out the UI
        Skin skin = asm.get(resLoc + "uiskin.json", Skin.class);

        //Play button
        TextureRegion start0 = new TextureRegion(asm.get(resLoc + "play_0.png", Texture.class));
        TextureRegion start1 = new TextureRegion(asm.get(resLoc + "play_1.png", Texture.class));

        //Options button (currently useless)
        TextureRegion opt0 = new TextureRegion(asm.get(resLoc + "options_0.png", Texture.class));
        TextureRegion opt1 = new TextureRegion(asm.get(resLoc + "options_1.png", Texture.class));

        //Title graphic
        TextureRegion title = new TextureRegion(asm.get(resLoc + "title.png", Texture.class));
        TextureRegion tile = new TextureRegion(asm.get(resLoc + "tileLand.png", Texture.class));

        final ImageButton startButt = new ImageButton(new TextureRegionDrawable(start0), new TextureRegionDrawable(start1));
        startButt.getImageCell().expand().fill();
        final ImageButton optionsButt = new ImageButton(new TextureRegionDrawable(opt0), new TextureRegionDrawable(opt1));
        optionsButt.getImageCell().expand().fill();

        Image titleGraphic = new Image(new TextureRegionDrawable(title));

        uiBody = new Table(skin);
        uiBody.setFillParent(true);

        uiBody.moveBy(0, 30*4);

        uiBody.row();

        //Add the title and the buttons

        Table uiMenu = new Table(skin);

        uiMenu.row().width(512).height(64);
        uiMenu.add(titleGraphic);
        uiMenu.row().padTop(16.0f);
        uiMenu.add(startButt).width(256).height(64).center();
        uiMenu.row().padTop(16.0f);
        uiMenu.add(optionsButt).width(256).height(64).center();

        startButt.setVisible(false);
        optionsButt.setVisible(false);

        uiBody.addAction(sequence(delay(13.4f), repeat(4, sequence(moveBy(0, -30, 0.1f, Interpolation.pow4Out), delay(0.75f))), run( new Runnable() {
            @Override
            public void run() {
                startButt.setVisible(true);
                optionsButt.setVisible(true);
            }
        })));

        uiBody.add(uiMenu);


        fpsLabel = new Label("VNW Version: " + version, skin);

        //Debug information for top-left
        Table debugT = new Table(skin);
        debugT.setFillParent(true);
        debugT.add(fpsLabel);
        debugT.left().top();

        fpsLabel.setVisible(false);

        //Add the scrolling landscape
        TiledGameObject land = new TiledGameObject(0, 0, tile);
        land.setMovementSpeed(2f);

        //Characters as they will be on the menu
        AnimatedObject player = new AnimatedObject(0, 0, 9, 2, charTex, true, 1/12f);
        AnimatedObject callum = new AnimatedObject(0, 0, 9, 2, asm.get(resLoc + "callum.atlas", TextureAtlas.class), true, 1/12f);

        player.translateWorld(40, land.getWorldHeight()-4);
        callum.translateWorld(70, land.getWorldHeight()-4);

        additions = new Group();
        additions.addActor(player);
        additions.addActor(land);
        additions.addActor(callum);

        //Add the two pillars
        uiStage.addActor(uiBody);
        uiStage.addActor(additions);
        uiStage.addActor(debugT);

        //Finally add the credit animation
        Table creditBody = new Table();
        creditBody.setFillParent(true);

        creditBody.row().expand().fill();

        Image titleMenu = new Image(new TextureRegionDrawable(new TextureRegion(asm.get(resLoc + "credit.png", Texture.class))));
        titleMenu.setTouchable(Touchable.disabled);

        creditBody.add(titleMenu);

        creditBody.addAction(sequence(delay(5f), fadeOut(1f)));

        uiStage.addActor(creditBody);

        //Pause with the escape button
        stage.addListener(new InputListener() {
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                if (keycode == Input.Keys.ESCAPE) {
                    setGameState(State.PAUSE);
                    return true;
                }
                return false;
            }
        });

        uiStage.addListener(new InputListener() {
            @Override
            public boolean keyDown(InputEvent event, int keycode) {
                if (keycode == Input.Keys.ESCAPE) {
                    setGameState(State.RUN);
                    return true;
                }
                return false;
            }
        });

        //Add listeners to buttons
        startButt.addListener(new ChangeListener() {
            public void changed(ChangeEvent event, Actor actor) {
                setGameState(State.RUN);
            }
        });

        mainMenu = asm.get(resLoc + "audio/menu.ogg", Music.class);
        mainMenu.setLooping(true);

        worldMin = stage.getWidth()/2;
        worldMax = level.getWidth()*scaleFactor-stage.getWidth()/2;

        setGameState(State.STOPPED);
    }

    @Override
    public void dispose() {
        stage.dispose();
    }

    @Override
    public void resize(int width, int height) {
        if (!init) return;

        stage.getViewport().update(width, height, true);
        uiStage.getViewport().update(width, height, true);

        scaleFactor = (stage.getViewport().getScreenHeight()/level.getHeight());
        level.setScale(scaleFactor);

        //Update all the actors
        for (Actor act : stage.getActors()) {
            if (act instanceof GameObject) {
                ((GameObject) act).updateBounds();
            }
            if (act instanceof Group) {
                for (Actor actg : ((Group) act).getChildren()) {
                    if (actg instanceof GameObject) {
                        ((GameObject) actg).updateBounds();
                    }
                }
            }
        }

        stage.getCamera().position.x = player.getX();

        worldMin = stage.getWidth()/2;
        worldMax = level.getWidth()*scaleFactor-stage.getWidth()/2;
    }

    @Override
    public void render() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        if (asm.update()) {

            if (!init) {
                init();
                init = true;
            }

            //Linear camera interpolation
            float distance = player.getX() - stage.getViewport().getCamera().position.x;
            if (distance < -camThreshold && stage.getViewport().getCamera().position.x > worldMin) {
                stage.getViewport().getCamera().translate(-2.0f*((float) Math.pow((Math.abs(distance)-camThreshold), camSnappiness)*Gdx.graphics.getDeltaTime()), 0, 0);
            }

            if (distance > camThreshold && stage.getViewport().getCamera().position.x < this.worldMax) {
                stage.getViewport().getCamera().translate(2.0f*((float) Math.pow((Math.abs(distance)-camThreshold), camSnappiness))*Gdx.graphics.getDeltaTime(), 0, 0);
            }

            if (state == State.RUN) {
                stage.act(Gdx.graphics.getDeltaTime());
            }

            stage.setDebugAll(this.debug);
            stage.draw();

            uiStage.act(Gdx.graphics.getDeltaTime());
            fpsLabel.setText("VNW Version: " + version + " | FPS: " + Gdx.graphics.getFramesPerSecond());
            //uiStage.draw();

        } else {
            float progress = asm.getProgress();

            loadProg.setText("Loading " + Math.round(progress * 100) + "%");
        }
        uiStage.draw();
    }

    @Override
    public void pause() {
        if (state != State.STOPPED)
            this.setGameState(State.PAUSE);
    }

    @Override
    public void resume() {
        //this.setGameState(State.RESUME);
    }

    public void setGameState(State s) {
        this.state = s;

        if (state == State.PAUSE || state == State.STOPPED) {
            uiBody.setVisible(true);
            Gdx.input.setInputProcessor(uiStage);
        } else {
            uiBody.setVisible(false);
            additions.setVisible(false);
            Gdx.input.setInputProcessor(stage);
        }

        if (state == State.STOPPED) {
            //Set the background to be the sky
            uiBody.setBackground((new TextureRegionDrawable(menuBackground)));
            additions.setVisible(true);

            //Play the main menu music
            mainMenu.play();

        } else {

            //Procedurally generated half black texture
            Pixmap colour = new Pixmap(16, 16, Pixmap.Format.RGBA8888);
            colour.setColor(0, 0, 0, 0.5f);
            colour.fill();

            uiBody.setBackground((new TextureRegionDrawable(new TextureRegion(new Texture(colour)))));
            colour.dispose();

            //Stop the menu music
            mainMenu.stop();
        }
    }

    public enum State
    {
        PAUSE,
        RUN,
        RESUME,
        STOPPED
    }
}


